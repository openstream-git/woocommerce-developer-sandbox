#!/bin/bash

read -p "Are you sure? This will overwrite the current database dump [y/Y]" -n 1 -r
echo 
if [[ $REPLY =~ ^[Yy]$ ]]
then
    docker exec -i woocommerce-sandbox-docker_db_1 /usr/bin/mysqldump -u wordpress --password=wordpress wordpress > ../.db/dump.sql
fi