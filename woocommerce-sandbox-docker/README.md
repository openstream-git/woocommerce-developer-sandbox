# WooCommerce Sandbox-Docker

Simple docker-compose setup with mysql and php7-nginx to run the sandbox wordpress locally. 

## Requirements

* Docker (https://www.docker.com/get-started)
* Only tested on linux, should run fine on Mac and Windows (minus the shell scripts)

## Gettings started

### Running the containers

Run `docker-compose up` to download the necessary images and run the two containers. 

Note: in the future you can use `docker-compose up -d` to run the containers in the background, after you verified that everything works.

### Add a hosts file entry

The webserver will be available on port 8080 (on localhost). Add this entry to `/etc/hosts`:

`127.0.0.1 woocommerce-sandbox.local`

Afterwards, access wordpress with this url:

http://woocommerce-sandbox.local:8080/

### Import database dump

A shell script helps with importing/exporting database:

`./import-db.sh`

### All done!

You are now ready to browse the wp installation. Use CTRL+C to stop the containers, or `docker-compose stop` when you started them in background mode.