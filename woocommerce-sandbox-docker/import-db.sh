#!/bin/bash

read -p "Are you sure? This will overwrite all data in the database [y/Y]" -n 1 -r
echo 
if [[ $REPLY =~ ^[Yy]$ ]]
then
    cat ../.db/dump.sql | docker exec -i woocommerce-sandbox-docker_db_1 /usr/bin/mysql -u wordpress --password=wordpress wordpress
fi